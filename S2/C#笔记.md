# C#前五章复习

### 1.C#的基本 语法

#### 1.1变量的命名规范 ：

前面可以是字母 ，下划线 

后面可以是数字， 字母 ，下划线 

变量名需要有意义

尽量使用驼峰命名法

不可以使用$符号

#### 1.2常量

- 常量的规则：
- 常量名均大写
- 常量名有意义
- 在声明的时候赋初始值，一旦确认下来之后不可以更改其值

#### 1.3Console类

输出：```Console.WriteLine("输出信息");``` 

输入：```Console.ReadLine();```

注意：输入返回的类型是string类型，如果需要用int类型接受，则需要强制转换

-----

输入int类型的值：```int num =int.Parse(Console.ReadLine());```

输入doule类型的值：```double d = double.Parse(Console.ReadLine());```

#### 1.4数组

声明数组的方式(四种)：

````
int [] nums = new int[5];

int [] nums = new int [5]{1,5,6,8,9}

int [] nums = {1,2,3,6};

int [] nums = new int[]{,2,5,6,8};
````



给数组中赋值：

```c#
nums[0] = 1;

nums[1] = 2;

nums[2] = 3;

nums[3] = 4;

nums[4] = 5;
```



遍历数组中的元素

```c#
int [] nums ={5,6,8,9,5,6};
//使用for循环遍历
for(int i =0;i<nums.Length,i++){
    Console.WriteLine(nums[i]);
}
for(int i =0 ;i<nums.Length;i++){
    nums[i] = Int.Parse(Console.ReadLine());
}

//使用foreach循环来遍历
foreach(int i in nums){
    Console.WriteLine(i);
}
foreach(int i in nums){
    i = Int32.Parse(Console.ReadLine());
}

//输出的结果：
/*
	5
	6
	8
	9
	5
	6
*/

```

#### 1.5条件语句

基本if语句

```
int a = 10;
if(a>10){
	//代码块
}
```

if-else语句

```
if(num>10){

}else{
	
}     
```

if-else if-else语句（多重if语句）

```
if(num>10){

}else if(num<=10){

}else{
	
}
```

嵌套if语句

```
if(num>10){
	if(num==10){
	
	}else{
		
	}	
}else{

}
```

#### 1.6循环

while循环

```
int i = 1;
while(i<=100){
	Console.WriteLine(i);
	i++;
}
```

do--while循环

```C# 
int i =1;
do{
	Console.WriteLine(i);
}while(i<=100);
```

for循环

```
for(int i = 1;i<=100;i++){
	Cosnole.WriteLine(i);
}
```

foreach循环

```
string [] strs  = {"张三","李四","王晓丹"};
foreach(string s in strs){
	Console.WrilteLine(s);
}
```

循环之间的特点是什么：

- while循环是先判断在执行
- do-while循环是先执行在判断，循环体至少会执行一次。
- for循环是在循环次数固定的情况下
- foreach是在遍历数组或者集合时方便使用

#### 1.6类和对象

类：一组相同属性和方法的集合，是一个抽象的概念

对象：描述客观事实存在的实体，能看得见，摸得着的

类和对象的关系是：抽象和具体的关系,由类实例化出对象，由对象抽象出类

```c#
//学生类
class Student{
	//姓名
	string name ;
	//性别
	string sex ;
	//班级
	string cname ;
	
	//显示学生的信息
	public void show(){
		Console.WrieLine("姓名是：{0}，性别是{1},所在班级是：{2}",name,sex,cname);
	}
}
//老师类
class Teacher{
    //姓名
    string name;
    //专业
    string zhuanye;
    
    //展示老师的信息
    public void show(){
        Console.WriteLine("姓名是：{0}，专业是：{1}",name,zhuanye);
        
    }
}

//测试类
class Test{
    static void Main(String []args){
        //创建学生类的对象
        Student stu = new Student();
        //给学生类的属性赋值
        stu.name = "刘同学";
        stu.age = 15;
        stu.cname = "计算机二班";
        //调用方法
        stu.show();
        
        //创建老师的对象
        Teacher tea = new Teacher();
        tea.name = "佟老师";
        tea.zhuanye = "管理";
        tea.show();
    }
}
```

#### 1.7传递方式

值传递：调用方法之后值不改变

```c#
public static void add(int num){
    num++;
}
static void Main(){
    int a = 10;
    add(a);
    Console.WriteLine(a);  //输出10
}
```



引用传递：调用方法之后，值会改变

```c#
public static void add(ref int num){
    num++;
}
static void Main(){
    int a = 10;
    add(ref a);
    Console.WriteLine(a);	//11
}
```

#### 1.8属性

面向对象的三大特征：封装，继承，多态

封装：对内隐藏细节，对外提供公共的方法，供其他类调用。

get:只读访问器

set:只写访问器

get,set:可读可写访问器

private:私有的，只能在本类中使用

public:公有的，可以在本类以外的类中使用。通过创建对象的方式（调用属性和方法）

```c#
class Student{
    private string name;
    public string Name{get;set;}
    private int age;
    public int Age{
        get{return this.age;}
        set{
            if(value<0 || value>=150){
                this.age = 18;
            }else{
                this.age = value;
            }
        }
    }
}
```

#### 1.9C#中的string类

给string中赋值：```string name = "张三";```

在指定的字符串中查找指定的字符（字符串）：

```c#
string name = "wanglili@163.com";
int name.IndexOf("@");
```

截取指定字符串中的字符串：

```c#
string name ="abcdefg";
name.SubString(0,5);//输出abcde
```

判断字符串是否为空

```c#
string name = "";
name.Equals("");
name.Equals(string,Empty);
name.Equals(null);
```

拆分字符串：

```c#
string music = "this is a pig";
string [] strs = music.Spilt('');
```

连接字符串

```c#
string name = "hello";
string [] strs = {"a","b","c"};
name.Join("-",strs);
```

格式化字符串：

```c#
string name = string.Format("姓名是：{0},性别是：{1}","李四","男");
```

数据类型之间的转换

将int、double,folat转换成string：```num.Tostring();```

将string转换成int:```int32.Parse("123");```

各种数据类型之间的转换：

string转换成double```Conver.ToDouble("23.63");```

#### 2.0C#中的方法

无参无返回值的方法

```c#
public void show(){
	Console.WriteLine("这是无参无返回值的方法");    
}
```

无参有返回值的方法

```c#
public int show()
{
    int num = 10;
    return num;
}
```

有参无返回值的方法

```C#
public void show(string name)
{
	Console.WriteLine("姓名是:"+name);
}
```

有参有返回值的方法

```c#
public string show(string name,string pwd){
    return name;
}
```

调用方法

​	先看是否在同一类中，如果在同类中，直接写方法名即可，否则需要创建方法所在类的对象，通过对象名.方法名的形式来调用。

调用无参无返回值的方法:

```c#
Test t = new Test();
t.show();
```

调用无参有返回值的方法：

```c#
Test t = new Test();
int a = t.show();
Console.WriteLine(a);
```

调用有参无返回值的方法：

```c#
Test t = new Test();
t.show("张三");
```

调用有参有返回值的方法

```c#
Test t = new Test();
string name  = t.show("admin","123456");
Console.WriteLine("姓名是："+name);
```

  



​                                                                                                                


